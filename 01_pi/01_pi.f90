program main
  implicit none
  real(8) x, y, pi, pi0, er
  integer :: n, i, im
  pi0 = 2.0d0 * acos(0.0d0) !cos(π/2)=0よりcos-1(0)=π/2
  n = 0
    print *, "円周率を推定する乱数生成数（試行回数）を入力"
    read (*,*) im

do i = 1, im
  call random_number(x) !random_number関数は0以上1未満のランダムな数字を呼び出す組み込み関数
  call random_number(y)
  if (x ** 2 + y ** 2 <= 1.0d0) n = n + 1 !乱数が原点を中心とする半径1の円(第一象限)の中に入った回数をカウント
enddo

pi = 4.0d0 * dble(n) / dble(im)  !dble関数は数字を倍精度にする関数、ここがモンテカルロ法の計算
er = (pi - pi0) / pi

write(*,*) '推定円周率は', pi
write(*,*) '誤差率は', er
  

end program main

