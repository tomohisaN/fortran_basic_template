program main
    use file_input
    use yousogousei_MT
    use cg
    use paraview
    implicit none
    
    integer(4) :: n, i, n2, m2, n3, m3, n4, m4, dimvec
    integer(4), allocatable :: elem(:,:)
    real(8) :: young, pos
    real(8), allocatable :: node(:,:), bc(:,:), u(:), f(:), Ke(:,:)

    call input(n, young, pos, n2, m2, node, n3, m3, elem, n4, m4, dimvec, bc, u, f)
    ! print*,"n,young,pos",n , young, pos

    call yousogousei(n, young, pos, Ke)
    ! print*, "要素剛性マトリックスKe①"
    ! do i=1,n
    !    print*, Ke(i,1:n)
    ! end do

    ! print*, "外力ベクトル"
    ! do i=1,n
    !     print*,f(i)
    ! end do

    call cg_solve(n, dimvec, Ke, f, u, n4, m4, bc)

    print*,"uベクトルは"
    do i=1,n
       print*,"u",i,"=",u(i)
    end do

    ! print*, "m3,n3", m3, n3
    call para(n2, m2, node, n3, m3, elem, u)

end program main   