module cg
    implicit none
    contains

    subroutine cg_solve(n, dimvec, Ke, f, u, n4, m4, bc)
    implicit none

    integer :: i, j, k, n, dimvec, n4, m4, count
     !integer :: fi1=40, fi2=41
    real(8) :: ips=1.0d-8, alpha, beta
    real(8), allocatable :: Ke(:,:), f(:), u(:), r0(:), r_damy(:), r(:), p0(:), p(:), A_x(:), A_p(:), bc(:,:)

    ! print*, "n=", n
    allocate(r0(n))
    allocate(r_damy(n))
    allocate(r(n))
    allocate(p0(n))
    allocate(p(n))
    allocate(A_x(n))
    allocate(A_p(n))

  ! open(fi1, file = 'matrixA.dat', status='old')
  ! do i =1,n
  !   read(fi1,*) Ke(i,1:n)
  !   Ke(i,1:n)=dble(Ke(i,1:n))
  ! enddo
  Print*, "全体剛性マトリックス"
  do i = 1,n
    print*,Ke(i,1:n)
  enddo
  ! close(fi1)

  ! open(fi2, file = 'vectorb.dat', status='old')
  ! do i =1,n
  !   read(fi2,*) b(i)
  !   b(i)=dble(b(i))
  ! enddo
  print*,"外力ベクトル"
  do i = 1,dimvec
    print*, "f", i, "=", f(i)
  enddo
  ! close(fi2)

  !境界条件付与
  do j=1,n4
    do i=1,n
      k=bc(j,1)*m4+bc(j,2)-m4
      Ke(k,i)=0.0d0
      Ke(k,k)=1.0d0
      f(i)=f(i)-Ke(i,k)*u(k)
      f(k)=u(k)
      Ke(i,k)=0.0d0
      Ke(k,k)=1.0d0
    end do
  end do
  print*,"境界条件を付与した全体剛性マトリックス"
  do i = 1,n
    print*,Ke(i,1:n)
  end do
  
  !ゼロクリア
  do i=1,n
    ! u(i)=0.0d0
    A_x(i)=0.0d0
    A_p(i)=0.0d0
  end do

  !共役勾配法開始
  do j=1,n
    do i=1,n
        A_x(i)=A_x(i)+Ke(i,j)*u(j)
    end do        
  end do
  ! print*,"Ax"
  ! do i=1,n
  !   print*,A_x(i)
  ! end do
  do i=1,n
    r0(i)=f(i)-A_x(i)
    r(i)=r0(i)
  end do
  ! print*,"r0"
  ! do i=1,n
  !   print*,r0(i)
  ! end do
  do i=1,n
    p0(i)=r0(i)
    p(i)=p0(i)
  end do
  

  count=0.0d0 
  do 
    count=count+1
    ! print*,"ループ", count, "回目"
    do i=1,n
      A_p(i)=0.0d0
    end do
    do j=1,n
       do i=1,n
          A_p(i)=A_p(i)+Ke(i,j)*p(j)
        end do        
    end do
    ! print*,"Ap"
    ! do i=1,n
    !   print*,A_p(i)
    ! end do
    alpha=DOT_PRODUCT (r,r)/DOT_PRODUCT (p,A_p)
    ! print*, "アルファ",alpha
    do k=1,n
      u(k)=u(k)+alpha*p(k)
      r_damy(k)=r(k)
      r(k)=r(k)-alpha*A_p(k)
    end do
    ! do i=1,n
    !   print*, "r     ", i, "=", r(i)
    !   print*, "r_damy", i, "=", r_damy(i)
    ! end do
    ! print*, "収束判定の値",DOT_PRODUCT (r,r)/DOT_PRODUCT(r0,r0)
    if(sqrt(DOT_PRODUCT (r,r))/sqrt(DOT_PRODUCT(r0,r0))<=ips) exit
    beta=DOT_PRODUCT (r,r)/DOT_PRODUCT (r_damy,r_damy)
    do i=1,n
      p(i)=r(i)+beta*p(i)
    end do    
    ! print*, "解は"
    ! do i=1,n
    !     print*,"x",i,"=",x(i)
    ! end do
  end do
 
  print*,"反復回数は", count,  "回で解は"
  ! do i=1,n
  !   print*,"u",i,"=",u(i)
  ! end do

end subroutine cg_solve
end module 