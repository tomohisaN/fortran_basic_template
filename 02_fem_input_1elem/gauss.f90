program main
    implicit none

    integer :: i, j, k, l, n, fi1=20, fi2=21
    real(8) :: p
    real(8), allocatable :: A(:,:) , b(:), x(:), tmp(:)

    open(fi1, file = 'gauss_matrix.dat', status='old')
    read(fi1,*) n
    ! print*, "この行列は", n, "×", n ,"行列"
    allocate (A(n,n))
    allocate (b(n))
    allocate (x(n))
    allocate (tmp(n))
    do i =1,n
       read(fi1,*) A(i,1:n)
       A(i,1:n)=dble(A(i,1:n))
    enddo
  Print*, "Aマトリックス"
  do i = 1,n
    print*,A(i,1:n)
  enddo
    close(fi1)

    open(fi2, file = 'gauss_vector.dat', status='old')
    do i =1,n
        read(fi2,*) b(i)
        b(i)=dble(b(i))
    enddo
  print*,"bベクトル"
  do i = 1,n
    print*,b(i)
  enddo
    close(fi2)

    !ガウスの消去法、前進消去
    do k=1,n-1
    !部分ピボットの処理
    l = k
    do i = k+1, n
        if ( abs(A(l,k)) < abs(A(i,k)) ) then
           l = i
        end if
    end do
    if ( l .ne. k ) then
        tmp = A(l,:)
        A(l,:) = A(k,:)
        A(k,:) = tmp
        tmp(1) = b(l)
        b(l) = b(k)
        b(k) = tmp(1)
    endif
        do i=k+1,n
            p=A(i,k)/A(k,k)
            do j=k+1,n
                A(i,j)=A(i,j)-p*A(k,j)
            end do
        b(i)=b(i)-p*b(k)
        end do
    end do

    !後退代入
    x(n)=b(n)/A(n,n)
    do i=n-1,1,-1
        x(i)=b(i)/A(i,i)
        do j=i+1,n
            x(i)=x(i)-A(i,j)*x(j)/A(i,i)
        end do
    end do

    print*, "解は"
    do i=1,8
       print*,"x" ,i ,"=" ,x(i)
    end do

end program main