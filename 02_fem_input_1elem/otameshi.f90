program main
  implicit none 
  
  integer(8):: x(3), y(3)
  x(1)=2
  x(2)=3
  x(3)=5
  y(1)=2
  y(2)=1
  y(3)=4
  print*, DOT_PRODUCT (x,y)
  print*, DOT_PRODUCT (y,y)
  
end program main