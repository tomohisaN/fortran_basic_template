program main
    use file_input
    use yousogousei_MT
    use zentaigousei_MT
    use cg
    use paraview
    implicit none
    
    integer(4) :: n, i, j, k, n2, m2, n3, m3, n4, m4, dimvec, elemlp
    integer(4), allocatable :: elem(:,:)
    real(8) :: young, pos
    real(8), allocatable :: node(:,:), bc(:,:), u(:), f(:), Ke(:,:,:), KMT(:,:)

    call input(n, young, pos, n2, m2, node, n3, m3, elem, n4, m4, dimvec, bc, u, f)

    allocate(Ke(n3,n,n))

    do elemlp=1,m3
        call yousogousei(n, young, pos, node, elem, elemlp, Ke)
    end do

    call zentai(n, m2, n3, m3, dimvec, elem, Ke, KMT)

    call cg_solve(dimvec, KMT, f, u, n4, m4, bc)

    print*,"uベクトルは"
    do i=1,dimvec
       print*,"u",i,"=",u(i)
    end do

    call para(n2, m2, node, n3, m3, elem, u)

end program main   