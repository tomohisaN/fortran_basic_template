module file_input
  implicit none
  contains

  subroutine input(n, young, pos, n2, m2, node, n3, m3, elem, n4, m4, dimvec, bc, u, f)
  implicit none
  
  integer(4) :: i, k, n, fi1=21, n2, m2, fi2=22,n3, m3, fi3=23,n4, m4, fi4=24, n5, m5, fi5=25, dimvec
  integer(4), allocatable :: elem(:,:)
  real(8) :: young, pos
  real(8), allocatable ::  node(:,:), bc(:,:), load(:,:), u(:), f(:)

  open(fi1, file = 'input.dat', status='old')
  read(fi1,*) young, pos
  print*,"ヤング率",young,"ポアソン比",pos
  close(fi1)

  open(fi2, file = 'node.dat', status='old')
  read(fi2,*) n2, m2
  allocate (node(n2,m2))
  print*, "節点数=", n2, "最大自由度数", m2
  ! print*, "size(node)=",size(node)
  dimvec=n2*m2
  do i =1,n2
    read(fi2,*) node(i,1:m2)
  end do
  do i = 1,n2
    print*,"節点",i,"x=",node(i,1),"y=",node(i,2)
  enddo
  close(fi2)

  open(fi3, file = 'elem.dat', status='old')
  read(fi3, *) m3, n3
  allocate (elem(m3,n3))
  print*,"要素数= ", m3, "1要素あたりの節点数", n3
  ! print*, "size(elem)=",shape(elem)                       
  do i =1,m3
    read(fi3,*) elem(i,1:n3)
  enddo
  do i = 1,m3
    print*,"節点番号",elem(i,1:n3)
  enddo
  close(fi3)

  n=m2*n3

  open(fi4, file = 'bc.dat', status='old')
  read(fi4, *) n4, m4
  allocate (bc(n4,3))
  print*,"境界(変位)条件数= ", n4, "最大自由度数= ", m4
  ! print*, "size(bc)=",size(bc)
  do i =1,n4
    read(fi4,*) bc(i,1:3)
  enddo
  do i=1,n4
    bc(i,1:2)= int(bc(i,1:2))
  end do
  do i = 1,n4
    print*,"節点",bc(i,1),"自由度方向",bc(i,2),"境界(変位)条件",bc(i,3)
  enddo
  close(fi4)

  allocate (u(dimvec))
  do i=1,dimvec
    u(i)=0.0d0
  end do
  do i=1,n4
    k=bc(i,1)*m4+bc(i,2)-m4
    u(k)=bc(i,3)
  end do
  print*, "変位ベクトル、0は未知数扱い"
  do i=1,dimvec
    print*, u(i)
  end do

  open(fi5, file = 'load.dat', status='old')
  read(fi5, *) n5, m5
  allocate (load(n5,3))
  print*,"境界(荷重)条件数= ", n5, "最大自由度数= ", m5
  ! print*, "size(load)=",size(load)
  do i =1,n5
    read(fi5,*) load(i,1:3)
  enddo
  do i=1,n5
    load(i,1:2)= int(load(i,1:2))
  end do
  do i = 1,n5
    print*,"節点",load(i,1),"自由度方向",load(i,2),"境界(荷重)条件",load(i,3)
  enddo
  close(fi5)

  allocate (f(dimvec))
  do i=1,dimvec
    f(i)=0.0d0
  end do
  do i=1,n5
    k=load(i,1)*m5+load(i,2)-m5
    f(k)=load(i,3)
  end do
  print*,"外力ベクトル"
  do i=1,dimvec
    print*, f(i)
  end do

  end subroutine input
end module

