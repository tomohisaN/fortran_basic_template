module paraview
    implicit none
    contains

    subroutine para(n2, m2, node, n3, m3, elem, u)
    implicit none  

    integer(4) :: i, j, k, n, n2, m2, n3, m3, fin=46, type=9
    integer(4), allocatable :: elem(:,:)
    ! real(8) :: 
    real(8), allocatable ::  node(:,:), u(:)

    open(fin, file="03_elem4.vtk", status="replace")
    write(fin,"('# vtk DataFile Version 4.1')")
    write(fin,"('03_elem result')")
    write(fin,"('ASCII')")
    write(fin,"('DATASET UNSTRUCTURED_GRID')")
    write(fin,"('POINTS',i9,' float')")n2
    do i =1,n2
    write(fin,*) node(i,1:m2), 0.0d0
    end do
    write(fin,*)"CELLS",m3,m3*(n3+1)
    do i=1, m3
    write(fin,*)n3, elem(i,1:n3)-1
    end do
    write(fin,*)"CELL_TYPES",m3
    do i=1,m3
       write(fin,*)type
    end do
    write(fin,*)"POINT_DATA",n2
    write(fin,"('VECTORS Point_Vector float')")
    k=-1
    do i =1,n2
        k=k+2
        write(fin,*)u(k),u(k+1),0.0d0
    end do
    
    close(fin)

end subroutine para
end module   