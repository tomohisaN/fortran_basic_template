module yousogousei_MT
    implicit none
    contains

    subroutine yousogousei(n, young, pos, node, elem, elemlp, Ke)
    implicit none  

    integer(4) :: i, j, k, n, elemlp
    integer(4), allocatable :: elem(:,:)
    real(8) :: young, pos
    real(8) :: Ke1(n,n), Ke2(n,n), Ke3(n,n), Ke4(n,n)
    real(8), allocatable :: node(:,:), Ke(:,:,:)

    call sum_BtDBdeJ(-0.57735d0,-0.57735d0)
    do i=1,n
        do j=1,n
            Ke1(i,j)=Ke(elemlp,i,j)
        end do
    end do
    ! do i=1,n
    !     print*, Ke1(i,1:n)
    ! end do    
    call sum_BtDBdeJ(-0.57735d0,0.57735d0)
    do i=1,n
        do j=1,n
            Ke2(i,j)=Ke(elemlp,i,j)
        end do
    end do
    ! do i=1,8
    !     print*, Ke2(i,1:n)
    ! end do
    call sum_BtDBdeJ(0.57735d0,0.57735d0)
    do i=1,n
        do j=1,n
            Ke3(i,j)=Ke(elemlp,i,j)
        end do
    end do
    ! do i=1,n
    !     print*, Ke3(i,1:n)
    ! end do
    call sum_BtDBdeJ(0.57735d0,-0.57735d0)
    do i=1,n
        do j=1,n
            Ke4(i,j)=Ke(elemlp,i,j)
        end do
    end do
    ! do i=1,n
    !     print*, Ke4(i,1:n)
    ! end do
   
   do i=1,n
        do j=1,n
            Ke(elemlp,i,j)=0.0d0
        end do
   end do
   do i=1,n
        do j=1,n
            Ke(elemlp,i,j)=Ke1(i,j)+Ke2(i,j)+Ke3(i,j)+Ke4(i,j)
        end do
   end do
!    print*, "要素剛性マトリックスKe", elemlp,"個目"
!    do i=1,n
!        print*, Ke(elemlp,i,1:n)
!    end do

contains
subroutine sum_BtDBdeJ(eta,cucy)
    real(8) :: eta, cucy, detJ
    real(8) :: dmt(3,3),  dnde_c(2,4), node_local(4,2), yako(2,2), in_yako(2,2), dndx_y(2,4), B(3,8), B_in(8,3), bk(8,3)

    do i=1,3
        dmt(i,1:3)=0.0d0
    end do
    dmt(1,1)=(young/(1+pos))*1/(1-pos)
    dmt(1,2)=(young/(1+pos))*pos/(1-pos)
    dmt(2,1)=(young/(1+pos))*pos/(1-pos)
    dmt(2,2)=(young/(1+pos))*1/(1-pos)
    dmt(3,3)=(young/(1+pos))*0.5
    ! print*,"弾性剛性マトリックスⅮ(平面応力状態)"
    ! do i=1,3
    !     print* ,dmt(i,1:3)
    ! end do 
    
     ! eta=-0.5d0
    ! cucy=-0.5d0
    dnde_c(1,1)=-0.25*(1-eta)
    dnde_c(1,2)=0.25*(1-eta)
    dnde_c(1,3)=0.25*(1+eta)
    dnde_c(1,4)=-0.25*(1+eta)
    dnde_c(2,1)=-0.25*(1-cucy) 
    dnde_c(2,2)=-0.25*(1+cucy)
    dnde_c(2,3)=0.25*(1+cucy)
    dnde_c(2,4)=0.25*(1-cucy)
    ! print*,"形状関数をηとξで偏微分した行列"
    ! do i=1,2
    !     print* ,dnde_c(i,1:4)
    ! end do

! print*, "elemlp=", elemlp

    do i=1,4
        do j=1,2
            node_local(i,j)=node(elem(elemlp,i),j)
        end do 
    end do

    ! print*, "ローカルな座標行列", elemlp, "要素目"
    ! do i=1,4
    !     print* ,node_local(i,1:2)
    ! end do

    do j=1,2
        do i=1,2
            yako(i,j)=0.0d0
            do k=1,4
                yako(i,j)=yako(i,j)+dnde_c(i,k)*node_local(k,j)
            end do        
        end do
    end do
    ! print*, "ヤコビアン行列J"    
    ! do i=1,2
    !     print* ,yako(i,1:2)
    ! end do
    detJ=yako(1,1)*yako(2,2)-yako(1,2)*yako(2,1)
    ! print*,"det|J|=", detJ

    in_yako(1,1)=yako(2,2)/detJ
    in_yako(1,2)=-yako(1,2)/detJ
    in_yako(2,1)=-yako(2,1)/detJ
    in_yako(2,2)=yako(1,1)/detJ
    ! print*, "ヤコビアン行列の逆行列-J"
    ! do i=1,2
    !     print* ,in_yako(i,1:2)
    ! end do  

    do i=1,4
        dndx_y(1,i)=in_yako(1,1)*dnde_c(1,i)+in_yako(1,2)*dnde_c(2,i)
        dndx_y(2,i)=in_yako(2,1)*dnde_c(1,i)+in_yako(2,2)*dnde_c(2,i)
    end do
    ! print*, "形状関数をxとyで偏微分した行列"
    ! do i=1,2
    !     print* ,dndx_y(i,1:4)
    ! end do 

    do i=1,8
        B(1:3,i)=0.0d0
    end do
    B(1,1)=dndx_y(1,1)
    B(3,1)=dndx_y(2,1)
    B(2,2)=dndx_y(2,1)
    B(3,2)=dndx_y(1,1)
    B(1,3)=dndx_y(1,2)
    B(3,3)=dndx_y(2,2)
    B(2,4)=dndx_y(2,2)
    B(3,4)=dndx_y(1,2)
    B(1,5)=dndx_y(1,3)
    B(3,5)=dndx_y(2,3)
    B(2,6)=dndx_y(2,3)
    B(3,6)=dndx_y(1,3)
    B(1,7)=dndx_y(1,4)
    B(3,7)=dndx_y(2,4)
    B(2,8)=dndx_y(2,4)
    B(3,8)=dndx_y(1,4)
    ! print*, "Bマトリックス"
    ! do i=1,3
    !     print* ,B(i,1:8)
    ! end do 

    do i=1,3
        do j=1,8
            B_in(j,i)=B(i,j)
        end do
    end do
    ! print*, "Bマトリックスの逆行列"
    ! do i=1,8
    !     print*, B_in(i,1:3)
    ! end do

    do j=1,3
        do i=1,8
            bk(i,j)=0.0d0
            do k=1,3
                bk(i,j)=bk(i,j)+B_in(i,k)*dmt(k,j)
            end do        
        end do
    end do
    ! print*,"BtDの計算結果"
    ! do i=1,8
    !     print*,bk(i,1:3)
    ! end do

    do j=1,8
        do i=1,8
            Ke(elemlp,i,j)=0.0d0
            do k=1,3
                Ke(elemlp,i,j)=Ke(elemlp,i,j)+bk(i,k)*B(k,j)
            end do        
        end do
    end do
    do j=1,8
        do i=1,8
            Ke(elemlp,i,j)=detJ*Ke(elemlp,i,j)
        end do
    end do
    ! print*, "η=",eta,"ξ=",cucy,"のBtDBdetJ"
    ! do i=1,8
    !    print*, Ke(i,1:8)
    ! end do
end subroutine sum_BtDBdeJ
end subroutine yousogousei
end module   