module zentaigousei_MT
  implicit none
  contains

  subroutine zentai(n, m2, n3, m3, dimvec, elem, Ke, KMT)
  implicit none
  
  integer(4) :: i, j, k, n, m2, n3, m3, dimvec, a, b
  integer(4), allocatable :: elem(:,:), elem_vec(:,:)
  real(8), allocatable ::  Ke(:,:,:), KMT(:,:), K_damy(:,:)

  allocate(elem_vec(m3,n3*m2))
  allocate(KMT(dimvec,dimvec))
  allocate(K_damy(dimvec,dimvec))

  do i=1,m3
    do j=1,n3
      k=0
      k=2*j-1
      elem_vec(i,k)=elem(i,j)*m2+1-m2
      k=0
      k=2*j
      elem_vec(i,k)=elem(i,j)*m2+2-m2
    end do
  end do
  ! print*,"節点番号をベクトル番号に直した行列"
  ! do i=1,m3
  !   print*,elem_vec(i,1:n3*m2)
  ! end do

  do i=1,dimvec
    do j=1,dimvec
        KMT(i,j)=0.0d0
    end do
  end do

  do k=1,m3
    do i=1,n
      do j=1,n
        do a=1,dimvec
        do b=1,dimvec
        K_damy(a,b)=0.0d0
        end do
        end do
        K_damy(elem_vec(k,i),elem_vec(k,j))=Ke(k,i,j)
        KMT(elem_vec(k,i),elem_vec(k,j))=KMT(elem_vec(k,i),elem_vec(k,j))+K_damy(elem_vec(k,i),elem_vec(k,j))
      end do
    end do
  end do

  ! print*,"全体剛性マトリックス"
  ! do i=1,dimvec
  !   print*,KMT(i,1:dimvec)
  ! end do
  

end subroutine zentai
end module