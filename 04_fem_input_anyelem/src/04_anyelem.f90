program main
    use make_mesh
    use file_input
    use yousogousei_MT
    use zentaigousei_MT
    use kyoukai_huyo
    use cg
    use taikaku_pcg
    use L2_norm_gosa
    use paraview
    implicit none
    
    integer(4) :: n, i, j, k, n2, m2, n3, m3, n4, m4, dimvec
    integer(4) :: count_rate=10000, time1, time2, time3, time4, time5, time6, time7, time8, time9
    integer(4), allocatable :: elem(:,:)
    real(8) :: young, pos
    real(8), allocatable :: node(:,:), bc(:,:), u(:), f(:), Ke(:,:,:), KMT(:,:)
    character(30) :: filename

    call system_clock(time1,count_rate)

    call mesh(filename)

    call system_clock(time2)

    call input(young, pos, n, n2, m2, node, n3, m3, n4, m4, elem, dimvec, bc, u, f)

    call system_clock(time3)

    call yousogousei(n, m2, m3, young, pos, node, elem, Ke)

    call system_clock(time4)

    call zentai(n, m2, n3, m3, dimvec, elem, Ke, KMT)

    call system_clock(time5)

    call huyo(dimvec, KMT, f, u, n4, m4, bc)

    call system_clock(time6)

    ! 線形ソルバ選択可能
    call cg_solve(dimvec, KMT, f, u) 
    ! call pcg_solve(dimvec, KMT, f, u)

    call system_clock(time7)

    call L2_norm(m2, m3, elem, node, u)

    call system_clock(time8)

    call para(n2, m2, node, n3, m3, elem, u, filename)

    call system_clock(time9)

    print*,"メッシュおよびファイル作成の時間", real(time2-time1)/count_rate, "秒"
    print*,"ファイル読みこみの時間", real(time3-time2)/count_rate, "秒"
    print*,"要素剛性マトリックス作成の時間", real(time4-time3)/count_rate, "秒"
    print*,"全体剛性マトリックス作成の時間", real(time5-time4)/count_rate, "秒"
    print*,"境界条件付与,ペナルティ数付与の時間", real(time6-time5)/count_rate, "秒"
    print*,"線形ソルバの時間", real(time7-time6)/count_rate, "秒"
    print*,"L2ノルム誤差計算の時間", real(time8-time7)/count_rate, "秒"
    print*,"vtkファイル作成の時間", real(time9-time8)/count_rate, "秒"
    print*,"シミュレータ全体の時間", real(time9-time1)/count_rate, "秒"

end program main   