module L2_norm_gosa
    use make_riron
    use calculate_L2

    implicit none

    contains
    subroutine L2_norm(m2, m3, elem, node, u)
    implicit none

    integer(4) :: m2, m3, elemlp
    integer(4), allocatable :: elem(:,:)
    real(8) :: sum_uth, sum_uthu, uth_any, uth_uany, e_L2
    real(8), allocatable :: node(:,:), u(:), ux_riron(:), uy_riron(:)
    

    call riron(ux_riron, uy_riron)

    sum_uth=0.0d0
    sum_uthu=0.0d0
    do elemlp=1,m3
        call L2(m2, node, u, ux_riron, uy_riron, elem, elemlp, uth_any, uth_uany)
        sum_uth=sum_uth+uth_any
        sum_uthu=sum_uthu+uth_uany
    end do
    e_L2=sqrt(sum_uthu)/sqrt(sum_uth)
    print*, "L2ノルム誤差は", e_L2


    end subroutine L2_norm
end module