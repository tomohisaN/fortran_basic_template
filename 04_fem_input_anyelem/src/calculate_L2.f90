module calculate_L2
    implicit none
    contains

    subroutine L2(m2, node, u, ux_riron, uy_riron, elem, elemlp, uth_any, uth_uany)
    implicit none  

    integer(4) :: i, j, k, m2, elemlp
    integer(4), allocatable :: elem(:,:)
    real(8) :: uxth_ux1, u_thx1, uxth_ux2, u_thx2, uxth_ux3, u_thx3, uxth_ux4, u_thx4
    real(8) :: uyth_uy1, u_thy1, uyth_uy2, u_thy2, uyth_uy3, u_thy3, uyth_uy4, u_thy4
    real(8) :: uth_any, uth_uany
    real(8), allocatable ::  u(:), ux_riron(:), uy_riron(:), node(:,:)

    call ninniu(-0.57735d0,-0.57735d0, uxth_ux1, u_thx1, uyth_uy1, u_thy1)

    call ninniu(-0.57735d0,0.57735d0, uxth_ux2, u_thx2, uyth_uy2, u_thy2)

    call ninniu(0.57735d0,0.57735d0, uxth_ux3, u_thx3, uyth_uy3, u_thy3)

    call ninniu(0.57735d0,-0.57735d0, uxth_ux4, u_thx4, uyth_uy4, u_thy4)

    uth_any=u_thx1+u_thx2+u_thx3+u_thx4+u_thy1+u_thy2+u_thy3+u_thy4
    uth_uany=uxth_ux1+uxth_ux2+uxth_ux3+uxth_ux4+uyth_uy1+uyth_uy2+uyth_uy3+uyth_uy4

    contains
    subroutine ninniu(eta, cucy, uxth_ux, u_thx, uyth_uy, u_thy)
        real(8) :: eta, cucy, detJ, uthx_local, uthy_local, ux_local, uy_local, uxth_ux, u_thx, uyth_uy, u_thy
        real(8) :: dnde_c(2,4), u_local(4,2), uth_local(4,2), node_local(4,2), yako(2,2)

        dnde_c(1,1)=-0.25*(1-eta)
        dnde_c(1,2)=0.25*(1-eta)
        dnde_c(1,3)=0.25*(1+eta)
        dnde_c(1,4)=-0.25*(1+eta)
        dnde_c(2,1)=-0.25*(1-cucy) 
        dnde_c(2,2)=-0.25*(1+cucy) 
        dnde_c(2,3)=0.25*(1+cucy)
        dnde_c(2,4)=0.25*(1-cucy)
        ! print*,"形状関数をηとξで偏微分した行列"
        ! do i=1,2
        !     print* ,dnde_c(i,1:4)
        ! end do

        do j=1,2
            do i=1,4
                u_local(i,j)=u(elem(elemlp,i)*m2-m2+j)
            end do
        end do
        ! print*, "ローカルな結果変位行列", elemlp, "要素目"
        ! do i=1,4
        !     print* ,u_local(i,1:2)
        ! end do

        do i=1,4
            uth_local(i,1)=ux_riron(elem(elemlp,i))
            uth_local(i,2)=uy_riron(elem(elemlp,i))
        end do
        ! print*, "ローカルな理論変位行列", elemlp, "要素目"
        ! do i=1,4
        !     print* ,uth_local(i,1:2)
        ! end do

        !形状関数で要素内の任意の変位を出す部分
        uthx_local=0.25*(1-cucy)*(1-eta)*uth_local(1,1)+0.25*(1+cucy)*(1-eta)*uth_local(2,1)&
                  +0.25*(1+cucy)*(1+eta)*uth_local(3,1)+0.25*(1-cucy)*(1+eta)*uth_local(4,1)
        uthy_local=0.25*(1-cucy)*(1-eta)*uth_local(1,2)+0.25*(1+cucy)*(1-eta)*uth_local(2,2)&
                  +0.25*(1+cucy)*(1+eta)*uth_local(3,2)+0.25*(1-cucy)*(1+eta)*uth_local(4,2)

        ux_local=0.25*(1-cucy)*(1-eta)*u_local(1,1)+0.25*(1+cucy)*(1-eta)*u_local(2,1)&
                +0.25*(1+cucy)*(1+eta)*u_local(3,1)+0.25*(1-cucy)*(1+eta)*u_local(4,1)
        uy_local=0.25*(1-cucy)*(1-eta)*u_local(1,2)+0.25*(1+cucy)*(1-eta)*u_local(2,2)&
                +0.25*(1+cucy)*(1+eta)*u_local(3,2)+0.25*(1-cucy)*(1+eta)*u_local(4,2)

        ! print*,"要素内の任意の変位" ,uthx_local,uthy_local,uthx_local-ux_local,uthy_local-uy_local

        do j=1,2
            do i=1,4
                node_local(i,j)=node(elem(elemlp,i),j)
            end do 
        end do
            
        ! print*, "ローカルな座標行列", elemlp, "要素目"
        ! do i=1,4
        !     print* ,node_local(i,1:2)
        ! end do
            
        do j=1,2
            do i=1,2
                yako(i,j)=0.0d0
                do k=1,4
                    yako(i,j)=yako(i,j)+dnde_c(i,k)*node_local(k,j)
                end do        
            end do
        end do
        ! print*, "ヤコビアン行列J"    
        ! do i=1,2
        !     print* ,yako(i,1:2)
        ! end do
        detJ=yako(1,1)*yako(2,2)-yako(1,2)*yako(2,1)
        ! print*,"det|J|=", detJ

        uxth_ux=((uthx_local-ux_local)*(uthx_local-ux_local))*detJ
        uyth_uy=((uthy_local-uy_local)*(uthy_local-uy_local))*detJ
        u_thx=(uthx_local*uthx_local)*detJ
        u_thy=(uthy_local*uthy_local)*detJ

    end subroutine ninniu
    end subroutine L2
end module