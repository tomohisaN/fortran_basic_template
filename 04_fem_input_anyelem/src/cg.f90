module cg
    implicit none
    contains

    subroutine cg_solve(dimvec, Ke, f, u)
    implicit none

    integer :: i, j, k, dimvec, count
    real(8) :: ips=1.0d-8, alpha, beta
    real(8), allocatable :: Ke(:,:), f(:), u(:), r0(:), r_damy(:), r(:), p0(:), p(:), A_x(:), A_p(:)

    allocate(r0(dimvec))
    allocate(r_damy(dimvec))
    allocate(r(dimvec))
    allocate(p0(dimvec))
    allocate(p(dimvec))
    allocate(A_x(dimvec))
    allocate(A_p(dimvec))

  !ゼロクリア
  do i=1,dimvec
    A_x(i)=0.0d0
    A_p(i)=0.0d0
  end do

  !共役勾配法開始
  do j=1,dimvec
    do i=1,dimvec
        A_x(i)=A_x(i)+Ke(i,j)*u(j)
    end do        
  end do

  do i=1,dimvec
    r0(i)=f(i)-A_x(i)
    r(i)=r0(i)
  end do

  do i=1,dimvec
    p0(i)=r0(i)
    p(i)=p0(i)
  end do

  count=0.0d0 
  do 
    count=count+1
    ! print*,"ループ", count, "回目"

    do i=1,dimvec
      A_p(i)=0.0d0
    end do
    do j=1,dimvec
       do i=1,dimvec
          A_p(i)=A_p(i)+Ke(i,j)*p(j)
        end do        
    end do
    alpha=DOT_PRODUCT (r,r)/DOT_PRODUCT (p,A_p)
    do k=1,dimvec
      u(k)=u(k)+alpha*p(k)
      r_damy(k)=r(k)
      r(k)=r(k)-alpha*A_p(k)
    end do
    ! print*, "収束判定の値",sqrt(DOT_PRODUCT (r,r))/sqrt(DOT_PRODUCT(r0,r0))
    if(sqrt(DOT_PRODUCT (r,r))/sqrt(DOT_PRODUCT(r0,r0))<=ips) exit
    beta=DOT_PRODUCT (r,r)/DOT_PRODUCT (r_damy,r_damy)
    do i=1,dimvec
      p(i)=r(i)+beta*p(i)
    end do    
    ! print*, "解は"
    ! do i=1,dimvec
    !     print*,"x",i,"=",x(i)
    ! end do
  end do
 
  print*,"反復回数は", count !,  "回で解は"
  ! do i=1,dimvec
  !   print*,"u",i,"=",u(i)
  ! end do

end subroutine cg_solve
end module 