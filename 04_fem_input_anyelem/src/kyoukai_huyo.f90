module kyoukai_huyo
    implicit none
    contains

    subroutine huyo(dimvec, Ke, f, u, n4, m4, bc)
    implicit none

    integer :: i, j, k, dimvec, n4, m4
    real(8), allocatable :: Ke(:,:), f(:), u(:), bc(:,:)

    !  Print*, "全体剛性マトリックス"
    !  do i = 1,dimvec
    !    print*,Ke(i,1:dimvec)
    !  enddo

    !境界条件付与
    do j=1,n4
        do i=1,dimvec
            k=bc(j,1)*m4+bc(j,2)-m4
            Ke(k,i)=0.0d0
            Ke(k,k)=1.0d0
            f(i)=f(i)-Ke(i,k)*u(k)
            f(k)=u(k)
            Ke(i,k)=0.0d0
            Ke(k,k)=1.0d0
        end do
    end do
    ! print*,"境界条件を付与した全体剛性マトリックス"
    ! do i = 1,dimvec
    !     print*,Ke(i,1:dimvec)
    ! end do
    ! print*,"境界条件を付与したfベクトル"
    ! do i = 1,dimvec
    !     print*,f(i)
    ! end do

    end subroutine huyo
end module 