module make_BtDBdetJ
    implicit none
    contains
    
    
    subroutine BtDBdeJ(n, m2, elemlp, young, pos, node, elem, eta, cucy, Ke_keizyou)
    implicit none

    integer(4) :: i, j, n, m2, elemlp, damy
    integer, allocatable :: elem(:,:)
    real(8) :: young, pos, eta, cucy, detJ
    real(8) :: dmt(3,3),  dnde_c(2,4), node_local(4,2), yako(2,2), in_yako(2,2), dndx_y(2,4), B(3,8), Bt(8,3), BtD(8,3), BtDB(8,8)
    real(8), allocatable :: node(:,:), Ke_keizyou(:,:)

    ! print*, n, m2, elemlp

    dmt=0.0d0
    dmt(1,1)=young/(1.0d0-pos*pos)
    dmt(1,2)=young*pos/(1.0d0-pos*pos)
    dmt(2,1)=young*pos/(1.0d0-pos*pos)
    dmt(2,2)=young/(1.0d0-pos*pos)
    dmt(3,3)=(young/(1.0d0+pos))*0.5d0
    ! print*,"弾性剛性マトリックスⅮ(平面応力状態)"
    ! do i=1,3
    !     print* ,dmt(i,1:3)
    ! end do 
    
    dnde_c=0.0d0
    dnde_c(1,1)=-0.25d0*(1-eta)
    dnde_c(1,2)=0.25d0*(1-eta)
    dnde_c(1,3)=0.25d0*(1+eta)
    dnde_c(1,4)=-0.25d0*(1+eta)
    dnde_c(2,1)=-0.25d0*(1-cucy) 
    dnde_c(2,2)=-0.25d0*(1+cucy)
    dnde_c(2,3)=0.25d0*(1+cucy)
    dnde_c(2,4)=0.25d0*(1-cucy)
    ! print*,"形状関数をξとηで偏微分した行列"
    ! do i=1,2
    !     print* ,dnde_c(i,1:4)
    ! end do

    node_local=0.0d0
    do j=1,m2
        do i=1,4
            damy=elem(elemlp,i)
            node_local(i,j)=node(damy,j)
        end do 
    end do

    ! print*, "ローカルな座標行列", elemlp, "要素目"
    !  do i=1,4
    !      print* ,node_local(i,1:m2)
    !  end do

    yako=0.0d0
    yako=MATMUL(dnde_c,node_local)
    ! print*, "ヤコビ行列J"    
    ! do i=1,2
    !     print* ,yako(i,1:2)
    ! end do

    detJ=0.0d0
    detJ=yako(1,1)*yako(2,2)-yako(1,2)*yako(2,1)
    ! print*,"det|J|=", detJ

    in_yako=0.0d0
    in_yako(1,1)=yako(2,2)/detJ
    in_yako(1,2)=-yako(1,2)/detJ
    in_yako(2,1)=-yako(2,1)/detJ
    in_yako(2,2)=yako(1,1)/detJ
    ! print*, "ヤコビ行列の逆行列-J"
    ! do i=1,2
    !     print* ,in_yako(i,1:2)
    ! end do  

    dndx_y=0.0d0
    do i=1,4
        dndx_y(1,i)=in_yako(1,1)*dnde_c(1,i)+in_yako(1,2)*dnde_c(2,i)
        dndx_y(2,i)=in_yako(2,1)*dnde_c(1,i)+in_yako(2,2)*dnde_c(2,i)
    end do
    ! print*, "形状関数をxとyで偏微分した行列"
    ! do i=1,2
    !     print* ,dndx_y(i,1:4)
    ! end do 

    B=0.0d0
    B(1,1)=dndx_y(1,1)
    B(1,3)=dndx_y(1,2)
    B(1,5)=dndx_y(1,3)
    B(1,7)=dndx_y(1,4)
    B(2,2)=dndx_y(2,1)
    B(2,4)=dndx_y(2,2)
    B(2,6)=dndx_y(2,3)
    B(2,8)=dndx_y(2,4)
    B(3,1)=dndx_y(2,1)
    B(3,2)=dndx_y(1,1)
    B(3,3)=dndx_y(2,2)
    B(3,4)=dndx_y(1,2)
    B(3,5)=dndx_y(2,3)
    B(3,6)=dndx_y(1,3)
    B(3,7)=dndx_y(2,4)
    B(3,8)=dndx_y(1,4)
    ! print*, "Bマトリックス"
    ! do i=1,3
    !     print* ,B(i,1:8)
    ! end do 

    Bt=0.0d0
    do j=1,8
        do i=1,3
            Bt(j,i)=B(i,j)
        end do
    end do
    ! print*, "Bマトリックスの転置行列"
    ! do i=1,8
    !     print*, Bt(i,1:3)
    ! end do

    BtD=0.0d0
    BtD=MATMUL(Bt,dmt)
    ! print*,"BtDの計算結果"
    ! do i=1,8
    !     print*,BtD(i,1:3)
    ! end do

    BtDB=0.0d0
    BtDB=MATMUL(BtD,B)

    Ke_keizyou=0.0d0
    do j=1,n
        do i=1,n
            Ke_keizyou(i,j)=detJ*BtDB(i,j)
        end do
    end do
    ! print*, "η=",eta,"ξ=",cucy,"のBtDBdetJ"
    ! do i=1,n
    !    print*, Ke_keizyou(i,1:n)
    ! end do

    end subroutine BtDBdeJ
end module