module make_mesh
    implicit none
    contains

    subroutine mesh(filename)
    implicit none

    integer(4) :: i, j, k, fis=33, find=34, fiel=35, fibc=36, fild=37, n_mesh, m_mesh
    integer(4), allocatable :: elem(:,:), eleposi(:,:)
    real(8) ::  vtc, hor, delta_x, delta_y, pow=2.0
    real(8), allocatable :: mesh_point(:,:,:)
    character(30) :: filename

    open(fis, file = 'start.dat', status='old')
    read(fis,*) n_mesh, m_mesh, vtc, hor, filename
    close(fis)
    print*, "縦の長さ", vtc, "横の長さ", hor
    print*,"縦の分割数",n_mesh,"横の分割数",m_mesh
    print*,"要素数", n_mesh*m_mesh, "節点数", (n_mesh+1)*(m_mesh+1) 
    
    allocate(mesh_point(n_mesh+1,m_mesh+1,2))
    do i=1,n_mesh+1
        do j=1,m_mesh+1 
            mesh_point(i,j,1:2)=0.0
            ! print*,"節点番号", i, j, mesh_point(i,j,1:2)
        end do
    end do
    delta_y=vtc/n_mesh
    delta_x=hor/m_mesh
    ! print*,"Δx=", delta_x,"Δy=", delta_y

    do i=1,n_mesh
        mesh_point(i+1,1,2)=mesh_point(i,1,2)+delta_y
    end do
    do i=1,n_mesh+1
        do j=1,m_mesh+1
            mesh_point(i,j,2)=mesh_point(i,1,2)
        end do
    end do
    do i=1,m_mesh
        mesh_point(1,i+1,1)=mesh_point(1,i,1)+delta_x
    end do
    do i=1,n_mesh+1
        do j=1,m_mesh+1
            mesh_point(i,j,1)=mesh_point(1,j,1)
        end do
    end do
    ! do i=1,n_mesh+1
    !     do j=1,m_mesh+1
    !         print*,"節点番号", i, j, mesh_point(i,j,1:2)
    !     end do
    ! end do
    open(find, file="node.dat", status="replace")
    write(find,*) (n_mesh+1)*(m_mesh+1), 2
    do i=1,n_mesh+1
        do j=1,m_mesh+1
            write(find,*) mesh_point(i,j,1:2)
        end do
    end do
    close(find)

    allocate(elem(n_mesh*m_mesh,4))
    elem(1,1)=1
    elem(1,2)=2
    elem(1,3)=elem(1,2)+m_mesh+1
    elem(1,4)=elem(1,1)+m_mesh+1
    do i=1,m_mesh
        do j=1,4
            elem(i+1,j)=elem(i,j)+1
        end do
    end do
    do i=1,n_mesh-1
        do j=1,m_mesh
            do k=1,4
                elem(j+i*m_mesh,k)=elem(j+(i-1)*m_mesh,k)+m_mesh+1
            end do
        end do
    end do
    ! do i = 1,n_mesh*m_mesh
    !     print*,elem(i,1:4)
    ! end do
    open(fiel, file="elem.dat", status="replace")
    write(fiel,*) n_mesh*m_mesh, 4
    do i=1,n_mesh*m_mesh
        write(fiel,*) elem(i,1:4)
    end do
    close(fiel)

    allocate(eleposi(n_mesh+1,m_mesh+1))
    eleposi(1,1)=1
    do i=1,m_mesh
        eleposi(1,i+1)=eleposi(1,i)+1
    end do
    do i=1,n_mesh
        do j=1,m_mesh+1
        eleposi(i+1,j)=eleposi(i,j)+m_mesh+1
        end do
    end do
    ! do i=1,n_mesh+1
    !     print*, eleposi(i,1:m_mesh+1)
    ! end do

    open(fibc, file="bc.dat", status="replace")
    write(fibc,*) n_mesh+m_mesh+2, 2
    do i=1,n_mesh+1
        write(fibc,*) eleposi(i,1), 1, 0.0
    end do
    do i=1,m_mesh+1
        write(fibc,*) eleposi(1,i), 2, 0.0
    end do
    close(fibc)

    open(fild, file="load.dat", status="replace")
    write(fild,*) n_mesh+1, 2
    pow=pow/n_mesh
    write(fild,*) eleposi(1,m_mesh+1), 1, pow/2
    do i=1,n_mesh-1
        write(fild,*) eleposi(i+1,m_mesh+1), 1, pow
    end do
    write(fild,*) eleposi(n_mesh+1,m_mesh+1), 1, pow/2
    close(fild)

    end subroutine mesh
end module  