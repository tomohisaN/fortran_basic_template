module make_riron
    implicit none
    contains

    subroutine riron(ux_riron, uy_riron)
    implicit none

    integer(4) :: i, j, k, fi1=25, fi2=26, n_all, m2
    real(8) :: young, pos, power=2.0d0
    real(8), allocatable :: node(:,:), ux_riron(:), uy_riron(:)

    open(fi1, file = 'input.dat', status='old')
    read(fi1,*) young, pos
    ! print*,"ヤング率",young,"ポアソン比",pos
    close(fi1)

    open(fi2, file = 'node.dat', status='old')
    read(fi2,*) n_all, m2
    allocate (node(n_all,m2))
    ! print*, "節点数=", n_all, "最大自由度数", m2
    ! print*, "size(node)=",size(node)
    do i =1,n_all
      read(fi2,*) node(i,1:m2)
    end do
    ! do i = 1,n_all
    !   print*,"節点",i,"x=",node(i,1),"y=",node(i,2)
    ! enddo
    close(fi2)

    allocate(ux_riron(n_all))
    allocate(uy_riron(n_all))

    do i=1,n_all
        ux_riron(i)=power*node(i,1)/young
        uy_riron(i)=-power*node(i,2)*pos/young
    end do

    ! do i=1,n_all
    !     print*, "節点", i, "のxの変位", ux_riron(i), "yの変位", uy_riron(i)
    ! end do
end subroutine
end module