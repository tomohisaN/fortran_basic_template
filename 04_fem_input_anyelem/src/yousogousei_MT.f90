module yousogousei_MT
    use make_BtDBdetJ

    implicit none
    contains

    subroutine yousogousei(n, m2, m3, young, pos, node, elem, Ke)
    implicit none  

    integer(4) :: i, j, k, n, m2, m3, elemlp
    integer(4), allocatable :: elem(:,:)
    real(8) :: young, pos
    real(8), allocatable :: node(:,:), Ke1(:,:), Ke2(:,:), Ke3(:,:), Ke4(:,:), Ke(:,:,:)

    allocate(Ke1(n,n))
    Ke1=0.0d0
    allocate(Ke2(n,n))
    Ke2=0.0d0
    allocate(Ke3(n,n))
    Ke3=0.0d0
    allocate(Ke4(n,n))
    Ke4=0.0d0
    allocate(Ke(m3,n,n))
    Ke=0.0d0


    do elemlp=1,m3 !要素数分ループを回す

    call BtDBdeJ(n, m2, elemlp, young, pos, node, elem, -0.57735d0, -0.57735d0, Ke1)
    ! print*,"ξ,ηパターン1"
    ! do i=1,n
    !     print*, Ke1(i,1:n)
    ! end do   

    call BtDBdeJ(n, m2, elemlp, young, pos, node, elem, -0.57735d0, 0.57735d0, Ke2)
    ! print*,"ξ,ηパターン2"
    ! do i=1,n
    !     print*, Ke2(i,1:n)
    ! end do

    call BtDBdeJ(n, m2, elemlp, young, pos, node, elem, 0.57735d0, 0.57735d0, Ke3)
    ! print*,"ξ,ηパターン3"
    ! do i=1,n
    !     print*, Ke3(i,1:n)
    ! end do

    call BtDBdeJ(n, m2, elemlp, young, pos, node, elem, 0.57735d0, -0.57735d0, Ke4)
    ! print*,"ξ,ηパターン4"
    ! do i=1,n
    !     print*, Ke4(i,1:n)
    ! end do
   
    do j=1,n
        do i=1,n
            Ke(elemlp,i,j)=Ke1(i,j)+Ke2(i,j)+Ke3(i,j)+Ke4(i,j)
        end do
    end do
!     print*, "要素剛性マトリックスKe", elemlp,"個目"
!    do i=1,n
!        print*, Ke(elemlp,i,1:n)
!    end do

    end do
    end subroutine yousogousei
end module   